
powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Datafeed\Debt\SunFact\Output\ -t txt -logfile _WCA_Conversion_Events

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Datafeed\Debt\SunFact\Output\ -t zip -logfile _WCA_Conversion_Events



REM Generate CONVT/CTCHG ResISIN codes

O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a COM_MY_EDI O:\AUTO\Scripts\mysql\WFI\Sun_Fact\CONVT.sql O:\Datafeed\Debt\SunFact\PreLDateSuf.txt :_CONVT _WCA_Conversion_Events

O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a COM_MY_EDI O:\AUTO\Scripts\mysql\WFI\Sun_Fact\CTCHG.sql O:\Datafeed\Debt\SunFact\PreLDateSuf.txt :_CTCHG _WCA_Conversion_Events


REM Append CONVT/CTCHG ResISIN codes to Port file

O:\AUTO\Scripts\vbs\FixedIncome\AppendTextFiles_New.vbs O:\Datafeed\Debt\SunFact\ CONVT O:\Datafeed\Debt\SunFact\ ISIN_PORT

O:\AUTO\Scripts\vbs\FixedIncome\AppendTextFiles_New.vbs O:\Datafeed\Debt\SunFact\ CTCHG O:\Datafeed\Debt\SunFact\ ISIN_PORT



REM Load Portfolio

O:\AUTO\Scripts\vbs\GENERIC\test\MySQL32.vbs COM_MY_EDI O:\AUTO\Scripts\mysql\WFI\Sun_Fact\SunFact_ISIN_Port.sql SunFact_ISIN_Port _WCA_Conversion_Events



REM Generate WFI Coverage Fields

O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a COM_MY_EDI O:\AUTO\Scripts\mysql\WFI\Sun_Fact\WFI_Coverage_Fields.sql O:\Datafeed\Debt\SunFact\PreLDateSuf.txt :_WFI_Coverage_Fields _WCA_Conversion_Events



REM Load WFI Coverage Fields


O:\AUTO\Scripts\vbs\GENERIC\test\MySQL32.vbs COM_MY_EDI O:\AUTO\Scripts\mysql\WFI\Sun_Fact\Load_WFI_Fields.sql SunFact_IssID_Port _WCA_Conversion_Events



REM Generate Sun Fact Feed

O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a COM_MY_EDI O:\AUTO\Scripts\mysql\WFI\Sun_Fact\Sun_Fact_Daily.sql O:\Datafeed\Debt\SunFact\Output\PreLDateSuf.txt :_WCA_Conversion_Events _WCA_Conversion_Events

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a zip -s O:\Datafeed\Debt\SunFact\Output\ -d YYYYMMDD_WCA_Conversion_Events -t txt -logfile _WCA_Conversion_Events

o:\auto\scripts\vbs\generic\ftps.vbs O:\Datafeed\Debt\SunFact\Output\ /Custom/Bahar/FixedInc/Harikrishnan/Conversion/YYYYMMDD.zip DOTNET Upload WCA_Conversion_Events :_WCA_Conversion_Events WCA_Conversion_Events

Email Alert

O:\AUTO\Scripts\vbs\FixedIncome\Emailer.vbs 32 none
